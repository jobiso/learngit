<?php
$file_name = "add-new-user.php";
include('header.php');
?>

<?php
$Dashboard_Section = "Add New Rider";
if ($User_Role == "Manager") {
	include('inc-sacco-dashboard.php');
} else {
	include('inc-dashboard.php');
}
?>  

<div class="row">
<div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><?php
								echo "$Dashboard_Section";
								?></h4>
                            </div>
                            <div class="content">

                            
							<form method="post" action="processors/actions-savenew.php" enctype="multipart/form-data">
								<?php
								
								if ($error){
									echo "<hr size=1 />
									<span style='color: red'><strong>SIGNUP ERROR:</strong> ";
									if ($error == "-1"){
										echo "Did you confirm your password?";
									}
									if ($error == "-2"){
										echo "Mobile No $Mobile_Number is already in use in the system. Please enter another one.";
										$Mobile_Number = "";
									}
									if ($error == "-3"){
										echo "ID No $ID_Number is already in use in the system. Please enter another one.";
										$ID_Number = "";
									}
									if ($error == "-4"){
										echo "Email $Email is already in use in the system. Please enter another one.";
										$Email = "";
									}
									echo "</span><hr size=1 />";
								}
								
								if ($error == "-4"){
									$optIndChecked = "";
									$optCorpChecked = "checked";
								} else {
									$optIndChecked = "checked";
									$optCorpChecked = "";											
								}
								
								echo "
								<div class='row' id='Div_Form_Individual'>
									<label for='name' class='col-md-3 control-label'>Name:</label>
									<div class='col-md-3'>
										<input type='text' class='form-control border-input' name='First_Name' placeholder='First Name' value='$First_Name' required>
									</div>
									<div class='col-md-3'>
										<input type='text' class='form-control border-input' name='Last_Name' placeholder='Last Name' value='$Last_Name' required>
									</div>
								</div>
								<div class='row'>
									<label for='mobile' class='col-sm-3 control-label'>City / Town:</label>
									<div class='col-md-3'>
										<input type='text' class='form-control border-input' name='Town' placeholder='' value='$Town' required>
									</div>
								</div>
								<div class='row'>
									<label for='mobile' class='col-sm-3 control-label'>County:</label>
									<div class='col-md-3'>
										<input type='text' class='form-control border-input' name='County' placeholder='' value='$County' required>
									</div>
								</div>
								<div class='row'>
									<label for='mobile' class='col-sm-3 control-label'>ID Number:</label>
									<div class='col-md-3'>
										<input type='text' class='form-control border-input' name='ID_Number' placeholder='' value='$ID_Number' required maxlength=11 title='Please enter a valid ID Number'>
									</div>
								</div>";
								if ($User_Role != "Manager") {
									echo "
									<div class='row'>
									<label for='email' class='col-sm-3 control-label'>Email:</label>
									<div class='col-md-4'>
										<input type='email' class='form-control border-input' name='Email' placeholder='Email' value='$Email' required>
									</div>
								</div>";
								}
								echo "
								<div class='row'>
									<label for='mobile' class='col-md-3 control-label'>Mobile Number:</label>
									<div class='col-md-3'>
										<input type='text' class='form-control border-input' name='Mobile_Number' placeholder='0722001100' value='$Mobile_Number' required>
									</div>
									<div class='col-md-5'> It has to be in this format 0700111222
									</div>
								</div>

								<div class='row'>
										<label for='mobile' class='col-md-3 control-label'>Upload Photo</label>
										<div class='col-md-7'>
										<div class='form-group'>
                                            
												<input type='hidden' name='MAX_FILE_SIZE' value='900000000000' />
                                                <input type='file' class='form-control border-input' name='Photo' placeholder=''><br>NOTE: Upload Jpegs of a maximum of 400X400 pixels.
                                            </div>
                                        </div>
								</div>
								";

								if ($User_Role == "Manager") {
								} else {
									echo "<div class='row' id='Div_Form_Company'>
									<label for='mobile' class='col-sm-3 control-label'>Sacco:</label>
									<div class='col-md-7'>
										<select class='form-control border-input' name='Sacco_Id'>";
										$Saccos_Array = Saccos::Get_Saccos();
										foreach ($Saccos_Array as $key => $val){											
											echo "<option value='$key'>$val[Sacco_Name] - $val[Sacco_Location]</option>";
										}
									echo "</select>
									</div>
								</div>";
								}	
										

								?>
								
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-7">
										<?php
											if ($User_Role == "Manager") {
												echo "<input type='hidden' name='Sacco_Id' value='$Sacco_Id' />";
											}

											$hidden_field = "add_new_user";													
											echo "
											<input type='hidden' name='rider_category' value='Category_A' />
											<input type='hidden' name='Agent_Id' value='$My_Customer_Id' />
											<input type='hidden' name='action' value='$hidden_field' />
											<input type='hidden' name='FileName' value='$FileName' />
											";
										?>
										
										<button type="submit" class="btn btn-block btn-primary">Add New User</button>
									</div>
								</div>
									 <div class="clearfix"></div>
							</form>                    


</div>
                    </div>
</div>
<?php
include('inc-mainpage-closer.php');
?>

<?php
include('footer.php');