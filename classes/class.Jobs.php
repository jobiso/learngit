<?php
class Jobs {

	const myKEYjob = Var_CONFIG_Mykeyjob;
	const filepath = Var_CONFIG_Basepath_System_Files;


	public static function Add_New_Order($Page_Flag="Normal_Add_Order") {

		$mysqli = DB::myconn();
		
		extract ($_POST);

		$Session_Array = Customers::GetSession();
		$My_Customer_Id = $Session_Array['Customer_Id'];
		$My_Client_Type = $Session_Array['Client_Type'];
		if (($My_Client_Type == "New Client") or ($My_Client_Type == "Existing Direct Client")){
			$INTERNAL_CLIENT_TYPE = "Client";
		} else {
			$INTERNAL_CLIENT_TYPE = "Agent";
		}

		$Importing_Period_From = Misc::ConvertDateToMySQL($Importing_Period_From);
		$Importing_Period_To = Misc::ConvertDateToMySQL($Importing_Period_To);
		$Date_Of_Shipment = Misc::ConvertDateToMySQL($Date_Of_Shipment);

		$Supplier = Misc::SanitizeVariables($Supplier);
		$Proforma_Invoice = Misc::SanitizeVariables($Proforma_Invoice);
		$Bill_of_Lading = Misc::SanitizeVariables($Bill_of_Lading);
		$Vessel = Misc::SanitizeVariables($Vessel);
		$From_Port = Misc::SanitizeVariables($From_Port);
		$To_Port = Misc::SanitizeVariables($To_Port);
		$Final_Destination = Misc::SanitizeVariables($Final_Destination);
		$IDF_Number = Misc::SanitizeVariables($IDF_Number);
		$Clearing_Agent = Misc::SanitizeVariables($Clearing_Agent);
		$Container_Max_No = Misc::SanitizeVariables($Container_Max_No);
		$Insured_Item_Details = Misc::SanitizeVariables($Insured_Item_Details);
		$Your_Ref = Misc::SanitizeVariables($Your_Ref);

		$Sum_Insured_KES = str_replace(',', '' , $Sum_Insured_KES);
		$Sum_Insured_USD = str_replace(',', '' , $Sum_Insured_USD);
		$Sum_Insured_KES_Plus_Profit = str_replace(',', '' , $Sum_Insured_KES_Plus_Profit);

		
		if ($Page_Flag == "Agent_Add_Order"){
			//$Agent_Customer_Id = $My_Customer_Id;
			$query = "INSERT INTO orders (Customer_Id, Order_Date, Supplier, Proforma_Invoice, IDF_Number, Shipment_Mode, Date_Of_Shipment, Excess, Product_Id, Insured_Item_Details, Clearing_Agent, Sum_Insured_KES, Sum_Insured_USD, Sum_Insured_KES_Plus_Profit, Container_Max_No, Packaging_Type, Agent_Id, Bill_of_Lading, From_Port, To_Port, Final_Destination, Importing_Period_From, Importing_Period_To, Vessel, Unique_Cargo_Ref_No, Your_Ref) VALUES ('$Agent_Customer_Id', NOW(), '$Supplier', '$Proforma_Invoice', '$IDF_Number', '$Shipment_Mode', '$Date_Of_Shipment', '$Excess', '$Product_Id', '".Misc::SanitizeVariables($Insured_Item_Details)."', '$Clearing_Agent', '$Sum_Insured_KES', '$Sum_Insured_USD', '$Sum_Insured_KES_Plus_Profit', '$Container_Max_No', '$Packaging_Type', '$My_Customer_Id', '$Bill_of_Lading', '$From_Port', '$To_Port', '$Final_Destination', '$Importing_Period_From', '$Importing_Period_To', '$Vessel', '$Unique_Cargo_Ref_No', '$Your_Ref')";
		} else {
			$query = "INSERT INTO orders (Customer_Id, Order_Date, Supplier, Proforma_Invoice, IDF_Number, Shipment_Mode, Date_Of_Shipment, Excess, Product_Id, Insured_Item_Details, Clearing_Agent, Sum_Insured_KES, Sum_Insured_USD, Sum_Insured_KES_Plus_Profit, Container_Max_No, Packaging_Type, Bill_of_Lading, From_Port, To_Port, Final_Destination, Importing_Period_From, Importing_Period_To, Vessel, Unique_Cargo_Ref_No, Your_Ref) VALUES ('$My_Customer_Id', NOW(), '$Supplier', '$Proforma_Invoice', '$IDF_Number', '$Shipment_Mode', '$Date_Of_Shipment', '$Excess', '$Product_Id', '".Misc::SanitizeVariables($Insured_Item_Details)."', '$Clearing_Agent', '$Sum_Insured_KES', '$Sum_Insured_USD', '$Sum_Insured_KES_Plus_Profit', '$Container_Max_No', '$Packaging_Type', '$Bill_of_Lading', '$From_Port', '$To_Port', '$Final_Destination', '$Importing_Period_From', '$Importing_Period_To', '$Vessel', '$Unique_Cargo_Ref_No', '$Your_Ref')";
		}

		
		
		if (mysqli_query($mysqli, $query)){
			$Order_Id = mysqli_insert_id($mysqli);
			$Padd = "MP";
			/*
			if (strlen($Order_Id) == 1){
				$Zeros = "00000";
			}
			if (strlen($Order_Id) == 2){
				$Zeros = "0000";
			}
			if (strlen($Order_Id) == 3){
				$Zeros = "000";
			}
			if (strlen($Order_Id) == 4){
				$Zeros = "00";
			}
			if (strlen($Order_Id) == 5){
				$Zeros = "0";
			}
			if (strlen($Order_Id) == 6){
				$Zeros = "";
			}
			*/

			$query0 = "SELECT MAX(Internal_Cover_Numbering) AS IntCount FROM `orders`";
			$result0 = mysqli_query($mysqli, $query0) or die(mysqli_error($mysqli));
			$row0 = mysqli_fetch_array($result0, MYSQLI_ASSOC);
			$Current_No = $row0[IntCount];
			//Logs::WriteLog("DEBUG", "1) Current_No $Current_No");
			$Current_No++;
			//Logs::WriteLog("DEBUG", "1) Current_No $Current_No");	
			if ($Current_No == 1){
				$Current_No = 10001;
			}
			
			$Open_Cover_No = $Padd.$Current_No;
			$query2 = "UPDATE orders SET Open_Cover_No = '$Open_Cover_No', Internal_Cover_Numbering = '$Current_No' WHERE Order_Id = '$Order_Id'";
			mysqli_query($mysqli, $query2);

			if (is_array($Item)){
				foreach ($Item as $keyval){
				if ($keyval){
					Orders::Add_New_Order_Detail($Order_Id, $keyval);					
					}
				}
			}			

			$CustomerArray = Customers::Get_Single_Customer($My_Customer_Id);
			if ($CustomerArray[Signup_Category] == "Corporate"){
				$Customer_Name = $CustomerArray[Company_Name];
			} elseif($CustomerArray[Signup_Category] == "Clearing Agent"){
				$Customer_Name = $CustomerArray[Company_Name]." [Contact Person: ".$CustomerArray[First_Name]." ".$CustomerArray[Last_Name]."]";
			} else {
				$Customer_Name = $CustomerArray[First_Name]." ".$CustomerArray[Last_Name];	
			}

			
			$To_Admin = "marine@intraafrica.co.ke, mital@intraafrica.co.ke, mwangi@intraafrica.co.ke";
			//$To_Admin = "jobokoth@gmail.com";

			$Subject_Admin = "Someone Has Submitted An Order In IntraAfrica Marine Portal";
			$Date = date("r");
			$Sum_Insured_KES_Formatted = number_format($Sum_Insured_KES);
			
			if ($INTERNAL_CLIENT_TYPE == "Agent"){
			$Message_Admin = "Hi Admin

$Customer_Name Clearing Agent has added a new marine certificate with cover note no. $Open_Cover_No

CUSTOMER ID: $My_Customer_Id
CUSTOMER NAME: $Customer_Name
ORDER NO: $Order_Id
SUM INSURED: Ksh $Sum_Insured_KES_Formatted

Kindly process it ASAP.
";				
			} else {
				
			$Message_Admin = "Hi Admin

Someone has submitted a new marine certificate with cover note no. $Open_Cover_No

CUSTOMER ID: $My_Customer_Id
CUSTOMER NAME: $Customer_Name
ORDER NO: $Order_Id
SUM INSURED: Ksh $Sum_Insured_KES_Formatted

Kindly process it ASAP.
";
		}
			
			$From_Email = "From: Marine Portal <marineportal@intraafrica.co.ke>";
			$Send_Status_Admin = Misc::sendMessage("Email", $To_Admin, $Subject_Admin, $Message_Admin, $From_Email);

			$uploaddir = self::filepath.'/';

			$FirstUpload = $Order_Id. "_IDF_". basename($_FILES['Scanned_IDF']['name']);

			$uploadfile1 = $uploaddir . $FirstUpload;
			if (move_uploaded_file($_FILES['Scanned_IDF']['tmp_name'], $uploadfile1)) {
				$firstfile = "worked";
				$query2 = "UPDATE orders SET Scanned_IDF = '$FirstUpload' WHERE Order_Id = '$Order_Id'";
				mysqli_query($mysqli, $query2);
			}
			
			$SecondUpload = $Order_Id. "_SIV_". basename($_FILES['Scanned_Supplier_Invoice']['name']);

			$uploadfile2 = $uploaddir . $SecondUpload;
			if (move_uploaded_file($_FILES['Scanned_Supplier_Invoice']['tmp_name'], $uploadfile2)) {
				$secondfile = "worked";
				$query2a = "UPDATE orders SET Scanned_Supplier_Invoice = '$SecondUpload' WHERE Order_Id = '$Order_Id'";
				mysqli_query($mysqli, $query2a);
			}

			Logs::WriteLog("ADD", "Successfully Added New Order $Order_Id");
			return $Order_Id;
		} else {
			$err = mysqli_error($mysqli);
			Logs::WriteLog("ERROR", "Unable to Add New Order CLASS: Orders >>> FUNCTION: Add_New_Order >>> QUERY: $query >>> ERROR $err");
			return -1;
		}		
	}



	public static function Get_Jobs($customer_id = 0, $Order = "ASC", $Filter = "", $Filter_Val = ""){
		$mysqli = DB::myconn();
		
		if ($customer_id == "X"){
			$customer_id = 0;
			//$query_filter = " WHERE Order_Status != 'Draft'";
		}
		
		$FilterField = "Customer_Id";
		if ($Page_Flag == "Normal_View_Orders"){
			$FilterField = "rider_id";
		}
		if ($Page_Flag == "Agent_View_Orders"){
			$FilterField = "Agent_Id";
		}

		$query = "SELECT *, DATE_FORMAT(requested_at,'%e %b %Y') AS Request_Date FROM jobs";
		if ($customer_id >= 1){
			$query .= " WHERE customer_id = '$customer_id'";
			if ($Filter != ""){
					if ($Filter == "requested_at") {
						$query .= " AND $Filter LIKE '$Filter_Val %'";
					} else {
						$query .= " AND $Filter = '$Filter_Val'";
					}
			}
		} else {			
			if ($Filter != ""){
					if ($Filter == "requested_at") {
						$query .= " WHERE $Filter LIKE '$Filter_Val %'";
					} else {
						$query .= " WHERE $Filter = '$Filter_Val'";
					}
			}
		}
		$query .= " ORDER BY id $Order";

		Logs::WriteLog("DEBUG", $query);
		//print $query;
		
		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$MyArray = array();
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			extract ($row);
			$CustomerArray = Users::Get_Single_User($customer_id);
			$Name = $CustomerArray['first_name']." ".$CustomerArray['last_name'];	
			$row['ShortName'] = $Name;
						
			$MyArray[$id] = $row;
		}
		return $MyArray;
	}


	public static function Get_Rider_Jobs($customer_id = 0, $Order = "ASC", $Filter = "", $Filter_Val = ""){
		$mysqli = DB::myconn();
		
		if ($customer_id == "X"){
			$customer_id = 0;
			//$query_filter = " WHERE Order_Status != 'Draft'";
		}
		
		$FilterField = "Customer_Id";
		if ($Page_Flag == "Normal_View_Orders"){
			$FilterField = "rider_id";
		}
		if ($Page_Flag == "Agent_View_Orders"){
			$FilterField = "Agent_Id";
		}

		$query = "SELECT *, DATE_FORMAT(requested_at,'%e %b %Y') AS Request_Date FROM jobs";
		if ($customer_id >= 1){
			$query .= " WHERE rider_id = '$customer_id'";
			if ($Filter != ""){
					if ($Filter == "requested_at") {
						$query .= " AND $Filter LIKE '$Filter_Val %'";
					} else {
						$query .= " AND $Filter = '$Filter_Val'";
					}
			}
		} else {			
			if ($Filter != ""){
					if ($Filter == "requested_at") {
						$query .= " WHERE $Filter LIKE '$Filter_Val %'";
					} else {
						$query .= " WHERE $Filter = '$Filter_Val'";
					}
			}
		}
		$query .= " ORDER BY id $Order";

		Logs::WriteLog("DEBUG", $query);
		//print $query;
		
		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$MyArray = array();
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			extract ($row);
			$CustomerArray = Users::Get_Single_User($customer_id);
			$Name = $CustomerArray[first_name]." ".$CustomerArray[last_name];	
			$row['ShortName'] = $Name;
						
			$MyArray[$id] = $row;
		}
		return $MyArray;
	}


	public static function Update(){
		extract ($_POST);
		$mysqli = DB::myconn();
		
		$query2 = "UPDATE jobs SET rider_id = '$rider_id', status = 'Assigned' WHERE id = '$job_id'";
		//	exit;
		mysqli_query($mysqli, $query2);

		Logs::WriteLog("UPDATE", "Successfully Updated Job $job_id Rider $rider_id ***  $query2 *** CLASS: ".__CLASS__." >>> FUNCTION: ".__FUNCTION__);

		return "TRUE";
	}



   
}

?>