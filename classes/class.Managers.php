<?php
class Managers {

	const myKEYjob = Var_CONFIG_Mykeyjob;
	const fullpath = '/home/nanariderz/public_html';

	public static function AddSession($row){
		extract ($row);
		session_start();
		$_SESSION['Customer_Id'] = $Customer_Id;
		$_SESSION['Email'] = $Email;
		$_SESSION['User_Group_Id'] = $User_Group_Id;
		$_SESSION['First_Name'] = $First_Name;
		$_SESSION['Last_Name'] = $Last_Name;
		$_SESSION['Mobile_Number'] = $Mobile_Number;
		$_SESSION['Currency'] = $Currency;
		$_SESSION['County'] = $County;
		$_SESSION['Country'] = $Country;
		$_SESSION['Company_Name'] = $Company_Name;
		$_SESSION['Address'] = $Address;
		$_SESSION['Town'] = $Town;
		$_SESSION['Post_Code'] = $Post_Code;
		$_SESSION['Client_Type'] = $Client_Type;
		$_SESSION['User_Role'] = $User_Role;
		$_SESSION['Sacco_Id'] = $Sacco_Id;
		
		return $Sacco_Id;
	}

	public static function UpdateSession(){
		extract ($_POST);
		session_start();
		$_SESSION['Sub_Account_Id'] = $Sub_Account_Id;
	}

	public static function GetSession(){
		session_start();
		return $_SESSION;
	}

	public static function DestroySession(){
		session_start();
		$Customer_Id = $_SESSION['Customer_Id'];
		
		/*
		$mysqli = DB::myconn();
		if ($INTERNAL_CLIENT_TYPE == "Client"){			
			$query2 = "UPDATE orders SET Order_Status = 'Pending' WHERE Order_Status = 'Draft' AND Customer_Id = '$Customer_Id'";
			mysqli_query($mysqli, $query2);
		}

		if ($INTERNAL_CLIENT_TYPE == "Agent"){			
			$query2 = "UPDATE orders SET Order_Status = 'Pending' WHERE Order_Status = 'Draft' AND Agent_Id = '$Customer_Id'";
			mysqli_query($mysqli, $query2);
		}
		*/

		unset($_SESSION['Customer_Id']);
		unset($_SESSION);
		session_destroy();
		session_unset();
		$_SESSION = array();
		return $f;

		


	}

	public static function Authenticate_Customer_Login($Email, $User_Pass) {

		$mysqli = DB::myconn();
		
		$query="SELECT * FROM customers WHERE Email='$Email' AND User_Pass=ENCODE('$User_Pass','".self::myKEYjob."') LIMIT 1";

		$list_count = DB::QueryCount($query);

		if($list_count > 0) {
			$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
			$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
			$Sacco_Id = Managers::AddSession($row);
			Logs::WriteLog("LOGIN", "Successfully Logged In $Sacco_Id", $Sacco_Id);
			$User_Group_Id = $row['User_Group_Id'];
			return $User_Group_Id;
		} else {
			$User = $Email."+".$User_Pass;
			$err = mysqli_error($mysqli);
			Logs::WriteLog("LOGIN", "Login NOT Successful for $Email CLASS: ".__CLASS__." >>> FUNCTION: ".__FUNCTION__." >>> QUERY: $query >>> ERROR $err", $User);
			return "FALSE";
		}
    }

	public static function Add_New_Manager() {

		$mysqli = DB::myconn();
		
		extract ($_POST);

		$First_Name = strtolower($First_Name);
		$First_Name = ucwords($First_Name);

		$Last_Name = strtolower($Last_Name);
		$Last_Name = ucwords($Last_Name);	
		
		
		$queryX = "SELECT Customer_Id FROM customers WHERE Mobile_Number='$Mobile_Number'";
		$list_countX = DB::QueryCount($queryX);

		$queryX1 = "SELECT Customer_Id FROM customers WHERE ID_Number='$ID_Number'";
		$list_countX1 = DB::QueryCount($queryX1);

		$queryX2 = "SELECT Customer_Id FROM customers WHERE Email='$Email'";
		$list_countX2  = DB::QueryCount($queryX2);

		$error_count = 0;
		$list_countX = 0;
		
		if($list_countX >= 1){
			Logs::WriteLog("ERROR", "Existing Mobile CLASS: ".__CLASS__." >>> FUNCTION: ".__FUNCTION__."");
			return -2;
		} elseif ($list_countX1 >= 1) {
			Logs::WriteLog("ERROR", "Existing ID Number CLASS: ".__CLASS__." >>> FUNCTION: ".__FUNCTION__."");
			return -3;
		} elseif ($list_countX2 >= 1) {
			Logs::WriteLog("ERROR", "Existing Email CLASS: ".__CLASS__." >>> FUNCTION: ".__FUNCTION__."");
			return -4;
		} else {
			$Session_Array = Managers::GetSession();
		
		    $query = "INSERT INTO customers (User_Pass, User_Group_Id, First_Name, Last_Name, Mobile_Number, Email, Signup_Date, ID_Number, Sacco_Id, User_Role) VALUES ((ENCODE('$User_Pass','".self::myKEYjob."')), '3', '$First_Name', '$Last_Name', '$Mobile_Number', '$Email', NOW(), '$ID_Number', '$Sacco_Id', 'Manager')";
			
			if (mysqli_query($mysqli, $query)){
				$Customer_Id = mysqli_insert_id($mysqli);
				
				Logs::WriteLog("ADD", "Successfully Added New Manager $Customer_Id");
				return $Customer_Id;
			} else {
				$err = mysqli_error($mysqli);
				Logs::WriteLog("ERROR", "Unable to Add New Manager CLASS: ".__CLASS__." >>> FUNCTION: ".__FUNCTION__." >>> QUERY: $query >>> ERROR $err");
				return -5;
			}
		}

		
	}
	
	public static function Get_Managers(){
		$mysqli = DB::myconn();
		
		$query="SELECT Email, DECODE(User_Pass,'".self::myKEYjob."') AS User_Pass_Decoded FROM customers";
		
		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$MyArray = array();
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			extract ($row);
			$MyArray[$Email] = $User_Pass_Decoded;
		}
		return $MyArray;
	}
	
	public static function Get_Customers($type = "Default", $Sacco_Id = 0){
		$mysqli = DB::myconn();
		
		if ($type == "Default") {
			$query="SELECT *, DATE_FORMAT(created,'%e %b %Y') AS Signup_Date FROM users";
		} else {
			$query="SELECT *, DATE_FORMAT(created,'%e %b %Y') AS Signup_Date FROM users WHERE type = '$type'";

			if ($Sacco_Id >= 1) {
				$query .= " AND Sacco_Id = '$Sacco_Id'";
			}
		}

		$query .= " ORDER BY user_id DESC";
		
		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$MyArray = array();
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			//extract ($row);
			$user_id = $row[user_id];
			$AverageRating = number_format(Customers::Get_Average_Rate($user_id));
			$row['AverageRating'] = $AverageRating;
			$MyArray[$user_id] = $row;
		}
		return $MyArray;
	}

	public static function Get_Single_Customer($user_id) {
		$mysqli = DB::myconn();
		
		$query="SELECT *, DECODE(psswd,'".self::myKEYjob."') AS User_Pass_Decoded FROM users WHERE user_id = '$user_id'";
		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$MyArray = array();
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
		
		return $row;
	}


	public static function Update_Customer(){
		extract ($_POST);
		$mysqli = DB::myconn();
		
		if ($My_Client_Type == "Super Admin"){
			$query2 = "UPDATE customers SET PIN_Number = '$PIN_Number', Mobile_Number = '$Mobile_Number', Wise_Id = '$Wise_Id', User_Pass = ENCODE('$User_Pass','".self::myKEYjob."') WHERE Customer_Id = '$Customer2Edit'";
		} else {
			$query2 = "UPDATE customers SET PIN_Number = '$PIN_Number', Mobile_Number = '$Mobile_Number', Wise_Id = '$Wise_Id' WHERE Customer_Id = '$Customer2Edit'";
		}
			
		mysqli_query($mysqli, $query2);

		return "TRUE";
	}

	//AverageRatings
	public static function Get_Average_Rate($user_id){
		extract ($_POST);
		$mysqli = DB::myconn();
		
		$query="SELECT rating FROM ratings WHERE user_id = '$user_id'";
		
		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$count = 0;
		$ratingsum = 0;
		
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$count++;
			$ratingsum += $row['rating'];
		}

		if ($count > 0) {
			$AverageRatings = $ratingsum / $count;
		}
	
		
		return $AverageRatings;
		
	}

   
}

?>