<?php
class Saccos {

	const myKEYjob = Var_CONFIG_Mykeyjob;
	const fullpath = '/home/nanariderz/public_html';

	public static function Add_New_Sacco() {

		$mysqli = DB::myconn();
		
		extract ($_POST);

		$Sacco_Name = Misc::SanitizeVariables($Sacco_Name);
		$Sacco_Location = Misc::SanitizeVariables($Sacco_Location);
		$Sacco_Chairman = Misc::SanitizeVariables($Sacco_Chairman);
		$Sacco_Phone = Misc::SanitizeVariables($Sacco_Phone);
		

		$query = "INSERT INTO saccos (Sacco_Name, Sacco_Location, Sacco_Chairman, Sacco_Phone, Signup_Date) VALUES ('$Sacco_Name', '$Sacco_Location', '$Sacco_Chairman', '$Sacco_Phone', NOW())";
			
		if (mysqli_query($mysqli, $query)){
			$Sacco_Id = mysqli_insert_id($mysqli);
			
			Logs::WriteLog("ADD", "Successfully Added New Sacco $Sacco_Id CLASS: ".__CLASS__." >>> FUNCTION: ".__FUNCTION__);
			return $Sacco_Id;
		} else {
			$err = mysqli_error($mysqli);
			Logs::WriteLog("ERROR", "Unable to Add New Sacco CLASS: ".__CLASS__." >>> FUNCTION: ".__FUNCTION__." >>> QUERY: $query >>> ERROR $err");
			return -1;
		}

		
	}


	public static function Get_Saccos(){
		$mysqli = DB::myconn();
		
		$query="SELECT *, DATE_FORMAT(Signup_Date,'%e %b %Y') AS Signup_Date FROM saccos";
		
		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$MyArray = array();
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			extract ($row);
			$MyArray[$Sacco_Id] = $row;
		}
		return $MyArray;
	}


	public static function Get_Sacco_Packages($Sacco_Id){
		$mysqli = DB::myconn();
		
		$query="SELECT * FROM sacco_packages WHERE Sacco_Id = '$Sacco_Id'";
		
		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$MyArray = array();
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			extract ($row);
			$MyArray[$Package_Id] = $row;
		}
		return $MyArray;
	}

	public static function Get_Single_Sacco($Sacco_Id) {
		$mysqli = DB::myconn();
		
		$query="SELECT Sacco_Name, Sacco_Location FROM saccos WHERE Sacco_Id = '$Sacco_Id'";
		$row = DB::ReturnSingleRow($query);
		
		return $row;
	}


	public static function Update_Customer(){
		extract ($_POST);
		$mysqli = DB::myconn();
		
		if ($My_Client_Type == "Super Admin"){
			$query2 = "UPDATE customers SET PIN_Number = '$PIN_Number', Mobile_Number = '$Mobile_Number', Wise_Id = '$Wise_Id', User_Pass = ENCODE('$User_Pass','".self::myKEYjob."') WHERE Customer_Id = '$Customer2Edit'";
		} else {
			$query2 = "UPDATE customers SET PIN_Number = '$PIN_Number', Mobile_Number = '$Mobile_Number', Wise_Id = '$Wise_Id' WHERE Customer_Id = '$Customer2Edit'";
		}
			
		mysqli_query($mysqli, $query2);

		return "TRUE";
	}

	public static function Update($boda_id){
		extract ($_POST);
		$mysqli = DB::myconn();
		
		$query2 = "UPDATE bodabodas SET User_Id = '$newrider_id' WHERE Bodaboda_Id = '$boda_id'";
		//exit;
		mysqli_query($mysqli, $query2);

		Logs::WriteLog("UPDATE", "Successfully Updated Boda $boda_id Rider $rider_id ***  $query2 *** CLASS: ".__CLASS__." >>> FUNCTION: ".__FUNCTION__);

		return "TRUE";
	}

	public static function Get_Sacco_Contacts($Sacco_Id){
		$mysqli = DB::myconn();
		
		$query="SELECT Sacco_Phone FROM saccos WHERE Sacco_Id = '$Sacco_Id'";
		
		$MyArray = array("254713854231");
		$row = DB::ReturnSingleRow($query);
		$MyArray[] = $row['Sacco_Phone'];
		
		$query2 = "SELECT Mobile_Number FROM customers WHERE Sacco_Id = '$Sacco_Id' AND User_Role = 'Manager'";
		$result2 = mysqli_query($mysqli, $query2) or die(mysqli_error($mysqli));
		
		while($row2 = mysqli_fetch_array($result2, MYSQLI_ASSOC)){
			extract ($row2);
			$MyArray[] = $Mobile_Number;
		}
		return $MyArray;
	}

   
}

?>