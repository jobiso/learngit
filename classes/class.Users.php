<?php
class Users {

	const myKEYjob = Var_CONFIG_Mykeyjob;
	const fullpath = '/home/nanariderz/public_html';

	public static function AddSession($row){
		extract ($row);
		session_start();
		$_SESSION['Customer_Id'] = $Customer_Id;
		$_SESSION['Email'] = $Email;
		$_SESSION['User_Group_Id'] = $User_Group_Id;
		$_SESSION['First_Name'] = $First_Name;
		$_SESSION['Last_Name'] = $Last_Name;
		$_SESSION['Mobile_Number'] = $Mobile_Number;
		$_SESSION['Currency'] = $Currency;
		$_SESSION['County'] = $County;
		$_SESSION['Country'] = $Country;
		$_SESSION['Company_Name'] = $Company_Name;
		$_SESSION['Address'] = $Address;
		$_SESSION['Town'] = $Town;
		$_SESSION['Post_Code'] = $Post_Code;
		$_SESSION['Client_Type'] = $Client_Type;
		$_SESSION['Signup_Category'] = $Signup_Category;
		$_SESSION['PIN_Number'] = $PIN_Number;
		
		return $Customer_Id;
	}

	public static function UpdateSession(){
		extract ($_POST);
		session_start();
		$_SESSION['Sub_Account_Id'] = $Sub_Account_Id;
	}

	public static function GetSession(){
		session_start();
		return $_SESSION;
	}

	public static function DestroySession(){
		session_start();
		

		$Customer_Id = $_SESSION['Customer_Id'];
		
		/*
		$mysqli = DB::myconn();
		if ($INTERNAL_CLIENT_TYPE == "Client"){			
			$query2 = "UPDATE orders SET Order_Status = 'Pending' WHERE Order_Status = 'Draft' AND Customer_Id = '$Customer_Id'";
			mysqli_query($mysqli, $query2);
		}

		if ($INTERNAL_CLIENT_TYPE == "Agent"){			
			$query2 = "UPDATE orders SET Order_Status = 'Pending' WHERE Order_Status = 'Draft' AND Agent_Id = '$Customer_Id'";
			mysqli_query($mysqli, $query2);
		}
		*/

		unset($_SESSION['Customer_Id']);
		unset($_SESSION);
		session_destroy();
		session_unset();
		$_SESSION = array();
		return $f;

		


	}

	public static function Authenticate_Customer_Login($Email, $User_Pass) {

		$mysqli = DB::myconn();
		
		$query="SELECT * FROM customers WHERE Email='$Email' AND User_Pass=ENCODE('$User_Pass','".self::myKEYjob."') LIMIT 1";

		$stmt = mysqli_prepare($mysqli, $query);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		
		$list_count=mysqli_stmt_num_rows($stmt);
		if($list_count > 0) {
			$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
			$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
			$Customer_Id = Customers::AddSession($row);
			Logs::WriteLog("LOGIN", "Successfully Logged In", $Customer_Id);
			$My_Client_Type = $row['Client_Type'];
			$RetArray = array($My_Client_Type);
			return $RetArray;
		} else {
			$User = $Email."+".$User_Pass;
			$err = mysqli_error($mysqli);
			Logs::WriteLog("LOGIN", "Login NOT Successful for $Email CLASS: Customers >>> FUNCTION: Authenticate_Customer_Login >>> QUERY: $query >>> ERROR $err", $User);
			return "FALSE";
		}
    }

	public static function Add_New_Customer() {
		$mysqli = DB::myconn();		
		extract ($_POST);

		$First_Name = strtolower($First_Name);
		$First_Name = ucwords($First_Name);

		$Last_Name = strtolower($Last_Name);
		$Last_Name = ucwords($Last_Name);	
		
		
		$queryX = "SELECT user_id FROM users WHERE mobile_number='$Mobile_Number'";
		$list_countX = DB::QueryCount($queryX);

		$error_count = 0;
		$Client_Type = "New Agent / Broker";
		$User_Pass = "447*358*8484*38484*jK**";
		$User_Pass_Confirm = $User_Pass;
		$list_countX = 0;
		
		if($list_countX >= 1){
			Logs::WriteLog("ERROR", "Existing Email CLASS: Customers >>> FUNCTION: Add_New_Customer");
			return -2;
		} else {
			$Session_Array = Customers::GetSession();
		
			$query = "INSERT INTO users (psswd, first_name, last_name, type, location, mobile_number, email, created) VALUES ('$password', '$First_Name', '$Last_Name', 'Rider', '$Town', '$Mobile_Number', '$Email', NOW())";
			
			
			if (mysqli_query($mysqli, $query)){
				$Customer_Id = mysqli_insert_id($mysqli);
				Logs::WriteLog("ADD", "Successfully Added New Customer $Customer_Id");
				return $Customer_Id;
			} else {
				$err = mysqli_error($mysqli);
				Logs::WriteLog("ERROR", "Unable to Add New Customer CLASS: Customers >>> FUNCTION: Add_New_Customer >>> QUERY: $query >>> ERROR $err");
				return -1;
			}
		}
		
	}

	//This function is used when the users are registering themselves via USSD
	public static function Basic_Add_New_User($Name, $MSISDN) {

		$mysqli = DB::myconn();		

		$pieces = explode(" ", $Name);

		$First_Name = strtolower($pieces[0]);
		$First_Name = ucwords($First_Name);

		$Last_Name = strtolower($pieces[1]);
		$Last_Name = ucwords($Last_Name);	
		
		$Mobile_Number = substr($MSISDN, 3);
				
		$query = "INSERT INTO users (first_name, last_name, type, mobile_number, created, status) VALUES ('$First_Name', '$Last_Name', 'Rider', '$Mobile_Number', NOW(), 'Inactive')";
			//exit;
			
			if (mysqli_query($mysqli, $query)){
				$user_id = mysqli_insert_id($mysqli);
				
				Logs::WriteLog("ADD", "Successfully Added New User $user_id");
				return $user_id;
			} else {
				$err = mysqli_error($mysqli);
				Logs::WriteLog("ERROR", "Unable to Add New User CLASS: ".__CLASS__." >>> FUNCTION: ".__FUNCTION__." >>> QUERY: $query >>> ERROR $err");
				return -1;
			}
		
	}



	public static function Get_Customers($type=""){
		$mysqli = DB::myconn();
		
		$query="SELECT *, DATE_FORMAT(created,'%e %b %Y') AS Signup_Date FROM users";
		
		if (($type == "Customer") or ($type == "Rider")) {
			$query .= " WHERE type = '$type'";
		}

		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$MyArray = array();
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			extract ($row);
			$MyArray[$user_id] = $row;
		}
		return $MyArray;
	}

	public static function Get_Single_User($user_id) {
		$mysqli = DB::myconn();
		
		$query="SELECT *, DECODE(psswd,'".self::myKEYjob."') AS User_Pass_Decoded FROM users WHERE user_id = '$user_id'";
		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$MyArray = array();
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
		
		return $row;
	}

	public static function Get_User_By_Phone($MNumber, $Filter = "True") {
		$mysqli = DB::myconn();

		$row = array();
		
		if ($MNumber != "") {
			if ($Filter == "True"){
				$query="SELECT * FROM users WHERE mobile_number = '$MNumber' AND status != 'Inactive'";	
				//Logs::WriteLog("DEBUG", "PART 1 $query");
			} else{
				$query="SELECT * FROM users WHERE mobile_number = '$MNumber'";
				//Logs::WriteLog("DEBUG", "PART 2 $query");
			}			
		} else {
			$query="SELECT * FROM users WHERE mobile_number = '$MNumber'";
			//Logs::WriteLog("DEBUG", "PART 3 $query");
		}

		$result = mysqli_query($mysqli, $query) or die(mysqli_error($mysqli));
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

		return $row;
	}


	public static function Update_User(){
		extract ($_POST);
		$mysqli = DB::myconn();
		//exit;
		
		$query2 = "UPDATE users SET status = '$status', category = '$category', Sacco_Id = '$Sacco_Id' WHERE user_id = '$user_id'";
		mysqli_query($mysqli, $query2);
		
		$uploaddir = self::fullpath.'/photos/';
		$Profile_Picture_Extension = ".jpg";
		$File_Name = $user_id.$Profile_Picture_Extension;
		$uploadfile = $uploaddir . $File_Name;

		if (move_uploaded_file($_FILES['Photo']['tmp_name'], $uploadfile)) {
			$true = "true";
			$query3 = "UPDATE users SET imagename = '$File_Name' WHERE user_id = '$user_id'";
			mysqli_query($mysqli, $query3);
		} else {
			//echo "<P>MOVE UPLOADED FILE FAILED!!</P>";
			//print_r(error_get_last());
			//exit;
		}

		return "TRUE";
	}


	public static function Update_User_Amount($user_id, $amount){
		extract ($_POST);
		$mysqli = DB::myconn();
		//exit;
		
		$query2 = "UPDATE users SET Daily_Payment = '$amount' WHERE user_id = '$user_id'";
		mysqli_query($mysqli, $query2);
		
		return "TRUE";
	}

	public static function Update_User_Generic($user_id, $Field, $Value){
		extract ($_POST);
		$mysqli = DB::myconn();
		//exit;
		
		$query = "UPDATE users SET $Field = '$Value' WHERE user_id = '$user_id'";
		mysqli_query($mysqli, $query);

		Logs::WriteLog("DEBUG", "$query", 0);
		
		return "TRUE";
	}

	public static function Delete_User($user_id){
		extract ($_POST);
		$mysqli = DB::myconn();
		
		$query = "DELETE FROM users WHERE user_id = '$user_id'";
			
		mysqli_query($mysqli, $query);

		$uploaddir = self::fullpath.'/photos/';
		$Profile_Picture_Extension = ".jpg";
		$File_Name = $user_id.$Profile_Picture_Extension;
		$uploadfile = $uploaddir . $File_Name;
		@unlink($uploadfile);


		return "TRUE";
	}

   
}

?>