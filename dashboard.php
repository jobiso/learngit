<?php
$Page = "Dashboard";
include('header.php');

$EnableDataTables = "True";
$Dashboard_Section = "Dashboard";
include('inc-dashboard.php');
?>  

<div class="row">
<?php
$Successful_Jobs_Array = Jobs::Get_Jobs(0, $Order, "status", "Completed");
$Successful_Count = count($Successful_Jobs_Array);

$Customers_Array = Users::Get_Customers("Customer");
$Customers_Count = count($Customers_Array);

$Riders_Array = Users::Get_Customers("Rider");
$Riders_Count = count($Riders_Array);

$Missed_Count_Array = Jobs::Get_Jobs(0, $Order, "status", "Unassigned");
$Missed_Count = count($Missed_Count_Array);
?>

                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="icon-big icon-warning text-center">
                                            <i class="ti-server"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="numbers">
                                            <p><a href="dashboard.php#Successful_Rides">Successful Rides</a></p>
                                            <?php
                                                print $Successful_Count
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="icon-big icon-success text-center">
                                            <i class="ti-wallet"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="numbers">
                                            <p><a href="view-users.php?User_Type=Customer">No of Customers</a></p>
                                            <?php
                                                print $Customers_Count
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-calendar"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="icon-big icon-danger text-center">
                                            <i class="ti-pulse"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="numbers">
                                            <p><a href="view-users.php?User_Type=Rider">No of Riders</a></p>
                                            <?php
                                                print $Riders_Count
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-timer"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="icon-big icon-info text-center">
                                            <i class="ti-twitter-alt"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="numbers">
                                            <p><a href="dashboard.php#Missed_Rides">Missed Rides</a></p>
                                            <?php
                                                print $Missed_Count
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


							
								
<?php
include('inc-mainpage-closer.php');
?>

<?php
include('footer.php');
?>