-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2020 at 12:20 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `learn_job_framework`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `Customer_Id` int(11) NOT NULL,
  `Wise_Id` int(11) NOT NULL,
  `User_Pass` blob NOT NULL,
  `User_Group_Id` smallint(6) NOT NULL,
  `Title_Id` smallint(6) NOT NULL,
  `First_Name` varchar(100) NOT NULL,
  `Last_Name` varchar(100) NOT NULL,
  `Company_Name` varchar(255) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Town` varchar(100) NOT NULL,
  `Post_Code` char(5) NOT NULL,
  `ID_Number` varchar(11) NOT NULL,
  `Status` enum('Enabled','Disabled') NOT NULL DEFAULT 'Enabled',
  `Email` varchar(255) NOT NULL,
  `Mobile_Number` varchar(100) NOT NULL,
  `Visibility` enum('Visible','Invisible') NOT NULL DEFAULT 'Visible',
  `County` varchar(255) NOT NULL,
  `Country` varchar(255) NOT NULL,
  `Signup_Date` datetime NOT NULL,
  `User_Role` enum('Manager','Admin') NOT NULL DEFAULT 'Manager',
  `Sacco_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`Customer_Id`, `Wise_Id`, `User_Pass`, `User_Group_Id`, `Title_Id`, `First_Name`, `Last_Name`, `Company_Name`, `Address`, `Town`, `Post_Code`, `ID_Number`, `Status`, `Email`, `Mobile_Number`, `Visibility`, `County`, `Country`, `Signup_Date`, `User_Role`, `Sacco_Id`) VALUES(8, 0, 0x26ec18b7506526eb, 1, 1, 'Victor', 'Gitonga', 'Nana Riderz', 'P.O. Box 47433', 'Nairobi', '00100', '', 'Enabled', 'superadmin@nanariderz.com', '0720-465807', 'Visible', 'Nairobi', '', '0000-00-00 00:00:00', '', 0);
INSERT INTO `customers` (`Customer_Id`, `Wise_Id`, `User_Pass`, `User_Group_Id`, `Title_Id`, `First_Name`, `Last_Name`, `Company_Name`, `Address`, `Town`, `Post_Code`, `ID_Number`, `Status`, `Email`, `Mobile_Number`, `Visibility`, `County`, `Country`, `Signup_Date`, `User_Role`, `Sacco_Id`) VALUES(9, 0, 0x26ec18b7506526eb, 3, 1, 'Victor', 'Gitonga', 'Safaricom Ltd', 'P.O. Box 47433', 'Nairobi', '00100', '', 'Enabled', 'saccoadmin@nanariderz.com', '0720-465807', 'Visible', 'Nairobi', '', '0000-00-00 00:00:00', '', 1);
INSERT INTO `customers` (`Customer_Id`, `Wise_Id`, `User_Pass`, `User_Group_Id`, `Title_Id`, `First_Name`, `Last_Name`, `Company_Name`, `Address`, `Town`, `Post_Code`, `ID_Number`, `Status`, `Email`, `Mobile_Number`, `Visibility`, `County`, `Country`, `Signup_Date`, `User_Role`, `Sacco_Id`) VALUES(10, 0, 0x3ec67afd4ecd7ddb5db095a3b4f67fd92b7886, 3, 0, 'John', 'Kamau', '', '', '', '', '26266363111', 'Enabled', 'johnkamau@gmail.com', '0728-282828', 'Visible', '', '', '2019-07-18 17:12:33', '', 0);
INSERT INTO `customers` (`Customer_Id`, `Wise_Id`, `User_Pass`, `User_Group_Id`, `Title_Id`, `First_Name`, `Last_Name`, `Company_Name`, `Address`, `Town`, `Post_Code`, `ID_Number`, `Status`, `Email`, `Mobile_Number`, `Visibility`, `County`, `Country`, `Signup_Date`, `User_Role`, `Sacco_Id`) VALUES(13, 0, 0xb151ffdfeb83b31991c521437684d1e000258def, 3, 0, 'Grace', 'Kamau', '', '', '', '', '33377777', 'Enabled', 'gracekamau@gmail.com', '0729-999999', 'Visible', '', '', '2019-07-18 17:19:38', 'Manager', 3);
INSERT INTO `customers` (`Customer_Id`, `Wise_Id`, `User_Pass`, `User_Group_Id`, `Title_Id`, `First_Name`, `Last_Name`, `Company_Name`, `Address`, `Town`, `Post_Code`, `ID_Number`, `Status`, `Email`, `Mobile_Number`, `Visibility`, `County`, `Country`, `Signup_Date`, `User_Role`, `Sacco_Id`) VALUES(14, 0, 0xc2462369dc9550ee6fb940ae27a364399232febc7845, 3, 0, 'Protus', 'Etende', '', '', '', '', '66266366363', 'Enabled', 'protusetende@gmail.com', '838383883838', 'Visible', '', '', '2019-07-18 23:24:26', 'Manager', 3);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `from_location` varchar(255) DEFAULT NULL,
  `from_latitude` double DEFAULT 0,
  `from_longitude` double DEFAULT 0,
  `dest_location` varchar(255) DEFAULT NULL,
  `dest_latitude` double DEFAULT 0,
  `dest_longitude` double DEFAULT 0,
  `customer_id` int(11) DEFAULT NULL,
  `requested_at` datetime DEFAULT NULL,
  `rider_id` int(11) NOT NULL DEFAULT 0,
  `started_at` datetime DEFAULT NULL,
  `ended_at` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `rating` double DEFAULT NULL,
  `insured` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(765, 'Kose Apartments', -1.32336, 36.79613, '', -1.3233580111878962, 36.796128787100315, 1, '2017-11-23 20:31:00', 723300650, '2017-11-23 20:34:46', '2017-11-23 20:34:50', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(766, 'Kose Ln', -1.32369, 36.7963, '', -1.323761575943155, 36.796121411025524, 1, '2017-11-23 20:35:03', 723300650, '2017-11-23 20:36:31', '2017-11-23 20:36:33', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(767, 'Kose Apartments', -1.32372, 36.79614, '', -1.3234425, 36.7961679687, 1, '2017-11-23 20:56:50', 723300650, NULL, '2017-11-23 21:35:20', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(768, 'International Organization Of Migration', -1.32355, 36.79913, 'Mombasa Road, Nairobi, Kenya', -1.30986, 36.827701399999995, 1, '2017-11-24 07:36:42', 723300650, NULL, '2017-11-24 05:34:18', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(769, 'Mombasa Road', -1.30625, 36.82736, NULL, 0, 0, 1, '2017-11-24 08:35:01', 723300650, NULL, '2017-11-24 05:36:16', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(770, 'Mombasa Road', -1.3221, 36.84226, NULL, 0, 0, 1, '2017-11-24 08:40:02', 723300650, NULL, '2017-11-24 05:40:39', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(771, 'Mombasa Road', -1.32663, 36.85066, '', -1.3266324479074239, 36.85066446661949, 1, '2017-11-24 08:46:45', 723300650, '2017-11-24 08:47:07', '2017-11-24 08:47:10', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(772, 'Mombasa Road', -1.33553, 36.89282, '', -1.3355336381271297, 36.892823092639446, 1, '2017-11-24 08:47:32', 723300650, '2017-11-24 08:48:47', '2017-11-24 08:49:08', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(773, 'Mombasa Road', -1.36665, 36.91447, '', -1.366646321371653, 36.91446717828512, 1, '2017-11-24 09:06:41', 723300650, '2017-11-24 09:07:21', '2017-11-24 09:07:22', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(774, 'Mombasa Road', -1.34684, 36.90238, NULL, 0, 0, 1, '2017-11-24 18:39:23', 723300650, NULL, '2017-11-24 15:40:14', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(775, 'Kose Ln', -1.3238, 36.79629, '', -1.3237987816606367, 36.796293407678604, 1, '2017-11-24 20:53:01', 0, '2017-11-24 20:55:21', '2017-11-24 20:55:22', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(776, 'Kose Ln', -1.32379, 36.79631, NULL, 0, 0, 1, '2017-11-24 20:55:38', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(777, 'Kose Ln', -1.3238, 36.79629, NULL, 0, 0, 1, '2017-11-24 21:14:39', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(778, 'Kose Apartments', -1.32372, 36.79618, '', -1.3237190072386802, 36.79618343710899, 1, '2017-11-24 21:16:04', 723300650, '2017-11-24 21:16:43', '2017-11-24 21:16:46', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(779, 'Kose Apartments', -1.32372, 36.79612, '', -1.3237240350384527, 36.79612006992102, 1, '2017-11-24 21:23:58', 723300650, '2017-11-24 21:26:55', '2017-11-24 21:26:56', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(780, 'Kose Ln', -1.32376, 36.79616, '', -1.3237562129568108, 36.796157620847225, 1, '2017-11-24 21:42:41', 723300650, '2017-11-24 21:44:11', '2017-11-24 21:44:13', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(781, 'Kose Apartments', -1.32372, 36.79619, '', -1.3236125, 36.7963085937, 1, '2017-11-24 22:12:38', 723300650, NULL, '2017-11-24 22:36:32', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(782, 'Kose Ln', -1.32377, 36.79618, '', -1.3237749834089774, 36.796181090176105, 1, '2017-11-25 07:16:05', 723300650, '2017-11-25 07:16:39', '2017-11-25 07:16:41', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(783, 'Kianjokoma - Kiriari Rd', -0.40099, 37.50036, '', -0.40098831539418495, 37.50035651028156, 1, '2017-11-25 15:14:21', 723300650, '2017-11-25 15:15:03', '2017-11-25 15:15:09', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(784, 'Kose Apartments', -1.32366, 36.79613, '', -1.3236573328938896, 36.79612912237644, 1, '2017-11-26 17:57:13', 723300650, '2017-11-26 17:58:08', '2017-11-26 17:58:10', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(785, 'Kose Ln', -1.3238, 36.7963, NULL, 0, 0, 1, '2017-11-26 18:00:09', 723300650, NULL, '2017-11-26 16:17:15', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(786, 'Kose Ln', -1.32349, 36.79632, '', -1.3234850469443467, 36.79631754755974, 1, '2017-11-26 19:23:57', 723300650, '2017-11-26 19:55:15', '2017-11-26 19:55:17', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(787, 'Kose Apartments', -1.32363, 36.79621, '', -1.3236325290808866, 36.796207912266254, 1, '2017-11-26 20:55:33', 723300650, '2017-11-26 20:57:33', '2017-11-26 20:57:39', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(788, 'Foot Path', -1.26508, 36.66656, NULL, 0, 0, 18, '2017-11-26 21:23:53', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(789, 'Southern Bypass', -1.27641, 36.66602, NULL, 0, 0, 18, '2017-11-26 21:30:50', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(790, 'Southern Bypass', -1.26972, 36.66555, NULL, 0, 0, 18, '2017-11-26 21:35:13', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(791, 'Kose Apartments', -1.3237, 36.79612, '', -1.3236958793595666, 36.7961210757494, 1, '2017-11-26 21:37:16', 723300650, '2017-11-26 21:37:48', '2017-11-26 21:37:49', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(792, 'Southern Bypass', -1.26972, 36.66555, NULL, 0, 0, 18, '2017-11-26 21:38:29', 723300650, NULL, '2017-11-26 18:40:57', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(793, 'Southern Bypass', -1.26995, 36.66555, NULL, 0, 0, 18, '2017-11-26 21:41:59', 713854231, NULL, '2017-11-27 12:17:39', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(794, 'Kose Ln', -1.3238, 36.7963, '', -1.3237984464740014, 36.79630447179079, 1, '2017-11-26 21:42:22', 723300650, '2017-11-26 21:44:21', '2017-11-26 21:44:35', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(795, 'Kose Ln', -1.32374, 36.79655, '', -1.3237384480644359, 36.79655224084854, 1, '2017-11-26 21:44:49', 723300650, '2017-11-26 22:04:55', '2017-11-26 22:04:56', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(796, 'Southern Bypass', -1.26995, 36.66555, NULL, 0, 0, 18, '2017-11-26 21:48:45', 713854231, NULL, NULL, 'Accepted', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(797, 'Kose Apartments', -1.32369, 36.79613, '', -1.3236921923063485, 36.79612845182419, 1, '2017-11-26 22:06:32', 723300650, '2017-11-26 22:13:30', '2017-11-26 22:13:31', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(798, 'International Organization Of Migration', -1.32373, 36.79917, NULL, 0, 0, 1, '2017-11-27 07:41:54', 723300650, NULL, '2017-11-27 04:42:46', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(799, 'International Organization Of Migration', -1.32352, 36.79919, 'Langata Rd', 0, 0, 1, '2017-11-27 07:43:21', 723300650, NULL, '2017-11-27 07:46:56', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(800, 'Kikuyu Rd', -1.25544, 36.66555, NULL, 0, 0, 18, '2017-11-27 11:23:24', 713854231, NULL, NULL, 'Accepted', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(801, 'Ngong Rd', -1.29944, 36.79188, NULL, 0, 0, 18, '2017-11-27 13:10:20', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(802, 'Ngong Rd', -1.29944, 36.79188, NULL, 0, 0, 18, '2017-11-27 13:10:31', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(803, 'A2', -1.10957, 37.01459, NULL, 0, 0, 31, '2017-11-30 16:47:58', 713854231, NULL, NULL, 'Assigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(804, 'Meru - Nairobi Hwy', -1.11862, 37.00849, NULL, 0, 0, 31, '2017-11-30 17:23:30', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(805, 'Waiyaki Way', -1.27323, 36.80875, NULL, 0, 0, 33, '2017-11-30 17:35:58', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(806, 'Hunters Rd', -1.22676, 36.9237, NULL, 0, 0, 35, '2017-11-30 23:27:16', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(807, 'Hunters Rd', -1.22676, 36.9237, NULL, 0, 0, 35, '2017-11-30 23:27:26', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(808, 'Uhuru Hwy', -1.2748, 36.81175, NULL, 0, 0, 30, '2017-12-05 11:34:02', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(809, 'Uhuru Hwy', -1.2748, 36.81175, NULL, 0, 0, 30, '2017-12-05 11:34:15', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(810, 'Gatundu-Juja Road', -1.10662, 37.01504, NULL, 0, 0, 31, '2017-12-05 14:45:18', 713854231, NULL, '2017-12-05 11:48:15', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(811, 'Mombasa Road', -1.36672, 36.91477, NULL, 0, 0, 1, '2017-12-05 14:58:48', 713854231, NULL, '2017-12-05 12:03:14', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(812, '748 Plaza', -1.3238, 36.79629, NULL, 0, 0, 1, '2017-12-05 21:05:39', 723300650, NULL, '2017-12-07 14:49:06', 'Cancelled', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(813, 'Mombasa Road', -1.36668, 36.9149, '', -1.3666848671581926, 36.91489599645138, 1, '2017-12-07 17:49:45', 723300650, '2017-12-07 17:50:04', '2017-12-07 18:07:25', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(814, 'Mombasa Road', -1.3672, 36.91515, 'Mombasa Road', 0, 0, 1, '2017-12-07 18:07:48', 723300650, NULL, '2017-12-07 18:19:46', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(815, 'Kose Apartments', -1.3235, 36.79617, '', -1.3234991247850303, 36.79617203772068, 1, '2017-12-10 08:31:03', 723300650, '2017-12-10 08:31:45', '2017-12-10 08:31:47', 'Completed', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(816, 'Kose Ln', -1.32328, 36.7963, NULL, 0, 0, 1, '2017-12-14 18:34:11', 713854231, NULL, NULL, 'Assigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(817, 'Kose Ln', -1.32328, 36.7963, NULL, 0, 0, 1, '2017-12-14 18:35:07', 723300650, NULL, NULL, 'Accepted', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(818, 'St Helena', -27.24789, -7.12438, NULL, 0, 0, 36, '2017-12-18 13:41:50', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(819, 'St Helena', -27.24789, -7.12438, NULL, 0, 0, 36, '2017-12-18 13:41:56', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(820, 'Kiungani Rd', -1.35704, 36.93855, NULL, 0, 0, 36, '2017-12-18 13:42:38', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(821, 'Oginga Oginga Avenue', -0.28444, 36.09296, NULL, 0, 0, 36, '2017-12-25 15:40:26', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(822, 'Oginga Oginga Avenue', -0.28444, 36.09296, NULL, 0, 0, 36, '2017-12-25 15:40:31', 0, NULL, NULL, 'Unassigned', NULL, 0);
INSERT INTO `jobs` (`id`, `from_location`, `from_latitude`, `from_longitude`, `dest_location`, `dest_latitude`, `dest_longitude`, `customer_id`, `requested_at`, `rider_id`, `started_at`, `ended_at`, `status`, `rating`, `insured`) VALUES(823, 'Landhies Rd', -0.28933, 36.09232, NULL, 0, 0, 36, '2017-12-25 15:42:26', 0, NULL, NULL, 'Unassigned', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rating` double DEFAULT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `saccos`
--

CREATE TABLE `saccos` (
  `Sacco_Id` int(11) NOT NULL,
  `Sacco_Name` varchar(255) NOT NULL,
  `Sacco_Location` varchar(100) NOT NULL,
  `Sacco_Chairman` varchar(50) NOT NULL,
  `Sacco_Phone` varchar(50) NOT NULL,
  `Signup_Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saccos`
--

INSERT INTO `saccos` (`Sacco_Id`, `Sacco_Name`, `Sacco_Location`, `Sacco_Chairman`, `Sacco_Phone`, `Signup_Date`) VALUES(1, 'Syokimau Sacco', 'Syokimau', 'Mr. Peter Karanja', '0711-222333', '2018-01-24');
INSERT INTO `saccos` (`Sacco_Id`, `Sacco_Name`, `Sacco_Location`, `Sacco_Chairman`, `Sacco_Phone`, `Signup_Date`) VALUES(2, 'Kikuyu Sacco', 'Kikuyu Town', 'Mr. Peter Karanja', '074444444', '2018-01-29');
INSERT INTO `saccos` (`Sacco_Id`, `Sacco_Name`, `Sacco_Location`, `Sacco_Chairman`, `Sacco_Phone`, `Signup_Date`) VALUES(3, 'Test Sacco', 'Tena Estate', 'Karanja Kibugi', '07238338838383', '2019-07-16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `psswd` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `type` enum('Rider','Attendant') DEFAULT 'Rider',
  `rider_category` enum('Category_A','Category_B','Category_C','Category_D') NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `mobile_number` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `ratings` double DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `firebase_id` text DEFAULT NULL,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  `longitude` double DEFAULT 0,
  `latitude` double DEFAULT 0,
  `work_status` varchar(15) DEFAULT 'Free',
  `accepted_job` datetime DEFAULT NULL,
  `availability` varchar(12) NOT NULL,
  `imagename` varchar(255) NOT NULL,
  `county` varchar(255) NOT NULL,
  `id_number` varchar(20) NOT NULL,
  `points` int(11) NOT NULL DEFAULT 0,
  `category` int(11) NOT NULL DEFAULT 3,
  `Sacco_Id` int(11) NOT NULL,
  `Daily_Payment` float NOT NULL,
  `User_PIN` tinyblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `middle_name`, `last_name`, `uname`, `psswd`, `salt`, `type`, `rider_category`, `location`, `mobile_number`, `email`, `created`, `ratings`, `last_login`, `firebase_id`, `status`, `longitude`, `latitude`, `work_status`, `accepted_job`, `availability`, `imagename`, `county`, `id_number`, `points`, `category`, `Sacco_Id`, `Daily_Payment`, `User_PIN`) VALUES(1, 'Pete', NULL, 'Makau', NULL, '', NULL, 'Rider', 'Category_A', 'Nairobi', 706042205, '', '2019-07-18 23:39:24', NULL, NULL, NULL, 'Active', 0, 0, 'Free', NULL, '', '', 'Nairobi', '26262626', 0, 3, 3, 0, '');
INSERT INTO `users` (`user_id`, `first_name`, `middle_name`, `last_name`, `uname`, `psswd`, `salt`, `type`, `rider_category`, `location`, `mobile_number`, `email`, `created`, `ratings`, `last_login`, `firebase_id`, `status`, `longitude`, `latitude`, `work_status`, `accepted_job`, `availability`, `imagename`, `county`, `id_number`, `points`, `category`, `Sacco_Id`, `Daily_Payment`, `User_PIN`) VALUES(4, 'Job', NULL, 'Okoth', NULL, NULL, NULL, 'Rider', 'Category_A', 'Nairobi', 706042205, 'jobokoth@gmail.com', '2020-04-17 13:14:17', NULL, NULL, NULL, 'Active', 0, 0, 'Free', NULL, '', '', 'Nairobi', 'ddddddddddd', 0, 3, 1, 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`Customer_Id`),
  ADD UNIQUE KEY `User_Id_2` (`Customer_Id`),
  ADD KEY `User_Id` (`Customer_Id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saccos`
--
ALTER TABLE `saccos`
  ADD PRIMARY KEY (`Sacco_Id`),
  ADD UNIQUE KEY `Sacco_Name` (`Sacco_Name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `Customer_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=824;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `saccos`
--
ALTER TABLE `saccos`
  MODIFY `Sacco_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
