<?php
$file_name = "add-new-user.php";
include('header.php');
?>

<?php
$Dashboard_Section = "Edit Rider's Details";
include('inc-dashboard.php');
?>  

<div class="row">
<div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><?php
								echo "$Dashboard_Section";
								?></h4>
                            </div>
                            <div class="content">

                            
							<form method="post" action="processors/actions-update.php" enctype="multipart/form-data">
								<?php
								
								if ($error){
									echo "<hr size=1 />
									<span style='color: red'><strong>SIGNUP ERROR:</strong> ";
									if ($error == "-1"){
										echo "Did you confirm your password?";
									}
									if ($error == "-2"){
										echo "Email $Email is already in use in the system. Please enter another one.";
										$Email = "";
									}
									if ($error == "-3"){
										echo "Please enter the first name and the last name.";
									}
									if ($error == "-4"){
										echo "Please enter the company name.";
									}
									echo "</span><hr size=1 />";
								}
								
								if ($error == "-4"){
									$optIndChecked = "";
									$optCorpChecked = "checked";
								} else {
									$optIndChecked = "checked";
									$optCorpChecked = "";											
								}

                                $User_Array = Users::Get_Single_User($user_id);
								
								$uploaddir = '../photos/';
								$Profile_Picture_Extension = ".jpg";
								$File_Name = $user_id.$Profile_Picture_Extension;
								$filename = $uploaddir . $File_Name;

								if (file_exists($filename)) {
									echo "
									<div class='row' id='Div_Form_Company'>
										<label for='mobile' class='col-sm-3 control-label'>Photo:</label>
										<div class='col-md-2'>
											<img src='$filename' width='100' height='120' border='0' alt=''>
										</div>
									</div>
									";
								}
								echo "
								<div class='row' id='Div_Form_Individual'>
									<label for='name' class='col-md-3 control-label'>Name:</label>
									<div class='col-md-3'>
										<input type='text' class='form-control border-input' name='First_Name' value='$User_Array[first_name]'>
									</div>
									<div class='col-md-3'>
										<input type='text' class='form-control border-input' name='Last_Name' value='$User_Array[last_name]'>
									</div>
								</div>
								<div class='row'>
										<label for='mobile' class='col-md-3 control-label'>Upload Photo</label>
										<div class='col-md-7'>
										<div class='form-group'>
                                            
												<input type='hidden' name='MAX_FILE_SIZE' value='900000000000' />
                                                <input type='file' class='form-control border-input' name='Photo' placeholder=''><br>NOTE: If a photo exists, it will be over-written. Upload Jpegs of a maximum of 400X400 pixels.
                                            </div>
                                        </div>
								</div>
								<div class='row' id='Div_Form_Company'>
									<label for='mobile' class='col-sm-3 control-label'>Status:</label>
									<div class='col-md-2'>
										<select class='form-control border-input' name='status'>";
											if ($User_Array[status] == "Active") {
												echo "<option value='Active' selected>Active</option>
														<option value='Inactive'>Inactive</option>";
											} else {
												echo "<option value='Active'>Active</option>
														<option value='Inactive' selected>Inactive</option>";
											}                                            
											echo "
                                        </select>
									</div>
								</div>
								<div class='row' id='Div_Form_Company'>
									<label for='mobile' class='col-sm-3 control-label'>Category:</label>
									<div class='col-md-2'>
										<select class='form-control border-input' name='category'>";
											$Categs_Array = array(
												'1' => 'Owner',
												'2' => 'Owner/Rider',
												'3' => 'Rider'
											);
											foreach ($Categs_Array as $k=>$v) {
												echo "<option value='$k'";
												if ($k == $User_Array['category']) {
													echo " selected";
												}
												echo ">$v</option>";
											}
											                                           
											echo "
                                        </select>
									</div>
								</div>

								<div class='row' id='Div_Form_Company'>
									<label for='mobile' class='col-sm-3 control-label'>Sacco:</label>
									<div class='col-md-7'>
										<select class='form-control border-input' name='Sacco_Id'>
											";
											$Saccos_Array = Saccos::Get_Saccos();

											if ($User_Array[Sacco_Id] == 0) {
												echo "<option value='0' selected>-</option>";
												foreach ($Saccos_Array as $key => $val){									
													echo "<option value='$key'>$val[Sacco_Name] - $val[Sacco_Location]</option>";
												}
											} else {
												foreach ($Saccos_Array as $key => $val){
													if ($key == $User_Array[Sacco_Id]) {
														echo "<option value='$key' selected>$val[Sacco_Name] - $val[Sacco_Location]</option>";
													} else {
														echo "<option value='$key'>$val[Sacco_Name] - $val[Sacco_Location]</option>";
													}
												}
											}
										echo "
                                        </select>
									</div>
								</div>
								";

								?>
								
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-7">
										<?php
											$hidden_field = "edit_user";													
											echo "
											<input type='hidden' name='user_id' value='$user_id' />
											<input type='hidden' name='action' value='$hidden_field' />
											<input type='hidden' name='FileName' value='$FileName' />
											";
										?>
										
										<button type="submit" class="btn btn-block btn-primary">Update User</button>
									</div>
								</div>
									 <div class="clearfix"></div>
							</form>                    


</div>
                    </div>
</div>
<?php
include('inc-mainpage-closer.php');
?>

<?php
include('footer.php');