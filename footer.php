            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>

                        <!-- <li>
                            <a href="http://www.creative-tim.com">
                                Creative Tim
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                               Blog
                            </a>
                        </li>
                        <li>
                            <a href="http://www.creative-tim.com/license">
                                Licenses
                            </a>
                        </li> -->
                    </ul>
                </nav>
				<div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, developed by <a href="http://www.disruptivehq.com">DisruptiveHQ Ltd</a>
                </div>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="assets/js/paper-dashboard.js"></script>

	<?php
	if ($Dashboard_Section == "Dashboard") {
	?>

	<script type="text/javascript">
    	$(document).ready(function(){

        	demo.initChartist();

        	$.notify({
            	icon: 'ti-gift',
            	message: "Welcome to <b>Nana Ridez Admin Section</b>"

            },{
                type: 'success',
                timer: 4000
            });

    	});
	</script>

	<?php
	}
	?>







	<?php
		if ($Page_Variable_Flag == "Enable Date Picker"){
			echo "<script src='datepicker/js/bootstrap-datepicker.js'></script>";
		?>
			<script type="text/javascript">
			<!--
				function SetKES() {
					var KSH;
					var MyRate;
					var USD;
					var a;
					a = document.AddMarineOrder.Sum_Insured_USD.value;
					a=a.replace(/\,/g,''); // 1125, but a string, so convert it to number
					USD=parseFloat(a,10);
					MyRate = parseFloat(document.AddMarineOrder.ExchangeRate.value);
					//USD = parseFloat(document.AddMarineOrder.Sum_Insured_USD.value);
					KSH = USD * MyRate;
					document.AddMarineOrder.Sum_Insured_KES.value = KSH.toFixed(2);				
					document.AddMarineOrder.Sum_Insured_KES_Plus_Profit.value = KSH.toFixed(2);

				}

				function FormatCurrency(ctrl) {
					//Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
					if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40)
					{
						return;
					}

					var val = ctrl.value;

					val = val.replace(/,/g, "")
					ctrl.value = "";
					val += '';
					x = val.split('.');
					x1 = x[0];
					x2 = x.length > 1 ? '.' + x[1] : '';

					var rgx = /(\d+)(\d{3})/;

					while (rgx.test(x1)) {
						x1 = x1.replace(rgx, '$1' + ',' + '$2');
					}

					ctrl.value = x1 + x2;
				}

				function CheckNumeric() {
					return event.keyCode >= 48 && event.keyCode <= 57;
				}
			//-->
			</script>
			<script>


	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
			$('#dp3').datepicker();
			$('#dp4').datepicker();
			$('#dp5').datepicker();
			
			
			

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
		});
	</script>

		<?php
		}


		if ($Dashboard_Section == "View Customers Marine Covers"){
		?> 


		 <script type="text/javascript">
		 <!--
			     $(document).ready(function(){
      var i=1;
     $("#add_row").click(function(){
      $('#addr'+i).html("<td><select name='Premium_Item_Ids[]' class='form-control'><option value='' selected='selected'></option><option value='5'>ITL Premium</option><option value='7'>PHCF Premium</option><option value='11'>Transhipment Premium</option><option value='18'>SRCC Premium</option><option value='28'>Marine Premium</option><option value='29'>War Premium</option><option value='30'>Stamp Duty</option></select></td><td><input type='text' class='form-control' name='Rate[]'></td><td><input type='text' class='form-control' name='Amount[]' placeholder='' onChange='SumPremium(this.value)'></td><td>&nbsp;</td>");

      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
      i++; 
  });
     $("#delete_row").click(function(){
    	 if(i>1){
		 $("#addr"+(i-1)).html('');
		 i--;
		 }
	 });

});
		 //-->
		 </script>

		 <script type="text/javascript">
		 <!--
			function SumPremium(In) {
					var CurrTotal;
					var NewTotal;
					CurrTotal = parseFloat(document.AdminOrderProcess.Total_Premium.value);
					NewTotal = CurrTotal + parseFloat(In);
					document.AdminOrderProcess.Total_Premium.value = NewTotal;				

				}
		 //-->
		 </script>

		 <?php
		}


		 if ($EnableDataTables == "True"){
			echo "
			<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>			
			";
			?>
			<!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script> -->
			<script type='text/javascript' src='https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js'></script>
			<script type='text/javascript'>
			<!--
				$(document).ready( function () {
					$('#table_id').DataTable();
				} );

				$(document).ready( function () {
					$('#table_id_missed').DataTable();
				} );
			//-->
			</script>
			<?php
		 }

		 if ($GoogleMap == "True") {
		 ?>
			
			<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeYkNzHcahf3rvA3d73NeYHh7bDXA&callback=initMap">
			</script>
		 <?php
		 }
		 ?>
</html>
