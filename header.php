<?php
include('inc-top-common-includes.php');
if ($Report_Id == 6) {
	$Step = 1;
	$GoogleMap = "True";
}
?>
<!doctype html>
<html lang="en">
<head>
	<?php
		if ($Page == "Dashboard") {
			echo "<meta http-equiv='refresh' content='60'>";
		}
	?>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Nana Ridez Admin Portal</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport' />
	<meta name="description" content="Nana Ridez Admin Portal">

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">
	<?php
		if ($Page_Variable_Flag == "Enable Date Picker"){
			echo "<link href='datepicker/css/datepicker.css' rel='stylesheet'>";
		}
		if ($EnableDataTables == "True"){
			//echo "<link rel='stylesheet' type='text/css' href='//cdn.datatables.net/1.10.12/css/jquery.dataTables.css'>";
			echo "<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css'/> 
				";
		}
		if ($file_name == "agent-add-customer.php"){
			if ($error == "-4"){
				echo "
				<style type='text/css'>
					#Div_Form_Individual {
						display: none
					}
				</style>
				";	

			} else {
				echo "
				<style type='text/css'>
					#Div_Form_Company {
						display: none
					}
				</style>
				";	
			}			
		}

		if ($GoogleMap == "True") {
		?>
		<style>
			  /* Always set the map height explicitly to define the size of the div
			   * element that contains the map. */
			  #map {
				height: 100%;
			  }
			  /* Optional: Makes the sample page fill the window. */
			  html, body {
				height: 100%;
				margin: 0;
				padding: 0;
			  }
		</style>
		<?php
		}
		?>

</head>
<?php
	if ($Page == "Dashboard") {
		echo "<body onload=\"demo.showNotification('top','right')\">";
	} else {
		echo "<body>";
	}
?>
<div class="wrapper">
	<div class="sidebar" data-background-color="black" data-active-color="danger">