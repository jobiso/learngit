<div class="sidebar-wrapper">
            <div class="logo">
                <a href="main.php" class="simple-text">
                    <img src="images/logo.png" width="230" height="77" alt="" />
                </a>
            </div>

            <ul class="nav">
			<?php
				$Side_Array = array(
					'dashboard.php' => array('Dashboard', 'ti-panel'),
					'add-new-user.php' => array('Add User', 'ti-user'),
					'view-users.php' => array('View Users', 'ti-id-badge'),
					'processors/actions-logout.php' => array('Logout', 'ti-arrow-left')
				);
				foreach ($Side_Array as $key => $val){
					if ($val[0] == $Dashboard_Section){
						echo "
						<li class='active'>
							<a href='$key'>
								<i class='$val[1]'></i>
								<p>$val[0]</p>
							</a>
						</li>
						";										
					} else {
						echo "
						<li>
							<a href='$key'>
								<i class='$val[1]'></i>
								<p>$val[0]</p>
							</a>
						</li>
						";										
					}

				}
			?>


                
            </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
					<?php
						$Hapa = $Dashboard_Section;
						if (!$Hapa) {
							$Hapa = "User Profile";
						}
						echo "<a class='navbar-brand' href='#'>$Hapa</a>";
					?>
                    
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
					<?php											
							if ($My_Email) {
								echo "
								<li>
									<a href='agent-main.php' class='dropdown-toggle' data-toggle='dropdown'>
										<i class='ti-user'></i>
										<p>Welcome $Welcome_Name</p>
									</a>
								</li>
								<li>
									<a href='dashboard.php' class='dropdown-toggle'>
										<i class='ti-home'></i>
										<p>Home</p>
									</a>
								</li>
								<li>
									<a href='processors/actions-logout.php' class='dropdown-toggle'>
										<i class='ti-arrow-left'></i>
										<p>Logout</p>
									</a>
								</li>
								";
							} else {
								echo "
								<li>
									<a href='./' class='dropdown-toggle' data-toggle='dropdown'>
										<i class='ti-home'></i>
										<p>Home</p>
									</a>
								</li>
								<li>
									<a href='pre-signup.php' class='dropdown-toggle' data-toggle='dropdown'>
										<i class='ti-pencil'></i>
										<p>Signup</p>
									</a>
								</li>
								<li>
									<a href='login.php' class='dropdown-toggle' data-toggle='dropdown'>
										<i class='ti-arrow-right'></i>
										<p>Login</p>
									</a>
								</li>
								";
							}
					?>
                    </ul>

                </div>
            </div>
        </nav>

		<div class="content">
            <div class="container-fluid">
        