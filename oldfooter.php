            <footer>
                <div class="footer">

                    <div class="copyright">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 center-text">
                                    <div class="copyright-text">Developed by DisruptiveHQ</div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        </div><!-- wrapper end -->

        <script type="text/javascript" src="js/_jq.js"></script>

        <script type="text/javascript" src="js/_jquery.placeholder.js"></script>






        <script src="js/activeaxon_menu.js" type="text/javascript"></script> 
        <script src="js/animationEnigne.js" type="text/javascript"></script> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/ie-fixes.js" type="text/javascript"></script> 
        <script src="js/jq.appear.js" type="text/javascript"></script> 
        <script src="js/jquery.base64.js" type="text/javascript"></script> 
        <script src="js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script> 
        <script src="js/jquery.cycle.js" type="text/javascript"></script> 
        <script src="js/jquery.cycle2.carousel.js" type="text/javascript"></script> 
        <script src="js/jquery.easing.1.3.js" type="text/javascript"></script> 
        <script src="js/jquery.easytabs.js" type="text/javascript"></script> 
        <script src="js/jquery.infinitescroll.js" type="text/javascript"></script> 
        <script src="js/jquery.isotope.js" type="text/javascript"></script> 
        <script src="js/jquery.prettyPhoto.js" type="text/javascript"></script> 
        <script src="js/jQuery.scrollPoint.js" type="text/javascript"></script> 
        <script src="js/jquery.themepunch.plugins.min.js" type="text/javascript"></script> 
        <script src="js/jquery.themepunch.revolution.js" type="text/javascript"></script> 
        <script src="js/jquery.tipsy.js" type="text/javascript"></script> 
        <script src="js/jquery.validate.js" type="text/javascript"></script> 
        <script src="js/jQuery.XDomainRequest.js" type="text/javascript"></script> 
        <script src="js/kanzi.js" type="text/javascript"></script> 
        <script src="js/retina.js" type="text/javascript"></script> 
        <script src="js/timeago.js" type="text/javascript"></script> 
        <script src="js/tweetable.jquery.js" type="text/javascript"></script>

		<?php
		if ($Page_Variable_Flag == "Enable Date Picker"){
			echo "<script src='datepicker/js/bootstrap-datepicker.js'></script>";
		?>
			<script type="text/javascript">
			<!--
				function SetKES() {
					var KSH;
					var MyRate;
					var USD;
					var a;
					a = document.AddMarineOrder.Sum_Insured_USD.value;
					a=a.replace(/\,/g,''); // 1125, but a string, so convert it to number
					USD=parseFloat(a,10);
					MyRate = parseFloat(document.AddMarineOrder.ExchangeRate.value);
					//USD = parseFloat(document.AddMarineOrder.Sum_Insured_USD.value);
					KSH = USD * MyRate;
					document.AddMarineOrder.Sum_Insured_KES.value = KSH.toFixed(2);				
					document.AddMarineOrder.Sum_Insured_KES_Plus_Profit.value = KSH.toFixed(2);

				}

				function FormatCurrency(ctrl) {
					//Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
					if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40)
					{
						return;
					}

					var val = ctrl.value;

					val = val.replace(/,/g, "")
					ctrl.value = "";
					val += '';
					x = val.split('.');
					x1 = x[0];
					x2 = x.length > 1 ? '.' + x[1] : '';

					var rgx = /(\d+)(\d{3})/;

					while (rgx.test(x1)) {
						x1 = x1.replace(rgx, '$1' + ',' + '$2');
					}

					ctrl.value = x1 + x2;
				}

				function CheckNumeric() {
					return event.keyCode >= 48 && event.keyCode <= 57;
				}
			//-->
			</script>
			<script>


	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
			$('#dp3').datepicker();
			$('#dp4').datepicker();
			$('#dp5').datepicker();
			
			
			

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
		});
	</script>

		<?php
		}


		if ($Dashboard_Section == "View Customers Marine Covers"){
		?> 


		 <script type="text/javascript">
		 <!--
			     $(document).ready(function(){
      var i=1;
     $("#add_row").click(function(){
      $('#addr'+i).html("<td><select name='Premium_Item_Ids[]' class='form-control'><option value='' selected='selected'></option><option value='5'>ITL Premium</option><option value='7'>PHCF Premium</option><option value='11'>Transhipment Premium</option><option value='18'>SRCC Premium</option><option value='28'>Marine Premium</option><option value='29'>War Premium</option><option value='30'>Stamp Duty</option></select></td><td><input type='text' class='form-control' name='Rate[]'></td><td><input type='text' class='form-control' name='Amount[]' placeholder='' onChange='SumPremium(this.value)'></td><td>&nbsp;</td>");

      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
      i++; 
  });
     $("#delete_row").click(function(){
    	 if(i>1){
		 $("#addr"+(i-1)).html('');
		 i--;
		 }
	 });

});
		 //-->
		 </script>

		 <script type="text/javascript">
		 <!--
			function SumPremium(In) {
					var CurrTotal;
					var NewTotal;
					CurrTotal = parseFloat(document.AdminOrderProcess.Total_Premium.value);
					NewTotal = CurrTotal + parseFloat(In);
					document.AdminOrderProcess.Total_Premium.value = NewTotal;				

				}
		 //-->
		 </script>

		 <?php
		}


		 if ($EnableDataTables == "True"){
			echo "
			<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>			
			";
			?>
			<!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script> -->
			<script type='text/javascript' src='https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js'></script>
			<script type='text/javascript'>
			<!--
				$(document).ready( function () {
					$('#table_id').DataTable();
					 
				} );
			//-->
			</script>
			<?php
		 }
		 ?>

    </body>
</html>
