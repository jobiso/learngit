<?php
include('inc-top-common-includes.php');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title>Nana Ridez Admin Portal</title>

        <!--[if lt IE 9]>
        <script type="text/javascript" src="js/ie-fixes.js"></script>
        <link rel="stylesheet" href="css/ie-fixes.css">
        <![endif]-->

        <meta name="description" content="Nana Ridez Admin Portal">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--- This should placed first off all other scripts -->




        <link rel="stylesheet" href="css/revolution_settings.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/axfont.css">
        <link rel="stylesheet" href="css/tipsy.css">
        <link rel="stylesheet" href="css/prettyPhoto.css">
        <link rel="stylesheet" href="css/isotop_animation.css">
        <link rel="stylesheet" href="css/animate.css">





        <link href='css/style.css' rel='stylesheet' type='text/css'> 
        <link href='css/responsive.css' rel='stylesheet' type='text/css'>

        <link href="css/skins/dark-blue.css" rel='stylesheet' type='text/css' id="skin-file">



        <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script> -->

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600' rel='stylesheet' type='text/css'>

        <!--[if lt IE 9]>
        <script type="text/javascript" src="js/respond.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="css/color-chooser.css">
		<?php
		if ($Page_Variable_Flag == "Enable Date Picker"){
			echo "<link href='datepicker/css/datepicker.css' rel='stylesheet'>";
		}
		if ($EnableDataTables == "True"){
			//echo "<link rel='stylesheet' type='text/css' href='//cdn.datatables.net/1.10.12/css/jquery.dataTables.css'>";
			echo "<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css'/> 
				";
		}
		if ($file_name == "agent-add-customer.php"){
			if ($error == "-4"){
				echo "
				<style type='text/css'>
					#Div_Form_Individual {
						display: none
					}
				</style>
				";	

			} else {
				echo "
				<style type='text/css'>
					#Div_Form_Company {
						display: none
					}
				</style>
				";	
			}			
		}
		?>
    </head>
    <body>




        <div id="wrapper"  >

            <div class="top_wrapper">
                

                <!-- Header -->
                <header id="header">
                    <div class="container">

                        <div class="row header">

                            <!-- Logo -->
                            <div class="col-xs-2 logo">
                                <a href="index.php">
                                    <img src="images/logo.png" height="80" alt="Kanzi HTML5 Template"/>
									
                                </a>
                            </div>
                            <!-- //Logo// -->


                            <!-- Navigation File -->
                            <div class="col-md-10">
                                <!-- Mobile Button Menu -->
                                <div class="mobile-menu-button">
                                    <i class="fa fa-list-ul"></i>
                                </div>
                                <!-- //Mobile Button Menu// -->



                                <nav>
                                    <ul class="navigation">
										<?php
											
										if ($My_Email) {
											echo "
											<li>
                                                <a href=''><span class='label-nav'>
                                                    <span style='color: #000'>Welcome $Welcome_Name</span>
                                                </span> </a>                                              
											</li>
											<li>
                                            <a href='main.php'>
                                                <span class='label-nav'>
                                                    <span style='color: #000000'>Home</span>
                                                </span>                                               
                                            </a>
											</li>
											<li>
												<a href='processors/actions-logout.php'>
													<span class='label-nav'>
														Logout
													</span>                                                
												</a>                                            
											</li>";
										} else {
											/*
											echo "
											<li>
                                            <a href='./'>
                                                <span class='label-nav'>
                                                    <span style='color: #000000'>Home</span>
                                                </span>                                               
                                            </a>
											</li>
											<li>
                                            <a href='pre-signup.php'>
                                                <span class='label-nav'>
                                                    Signup
                                                </span>                                                
                                            </a>                                            
											</li>
											<li>
												<a href='login.php'>
													<span class='label-nav'>
														Login
													</span>                                                
												</a>                                            
											</li>";
											*/
										}
										?>
                                        
                                        
                                    </ul>

                                </nav>

                                <!-- Mobile Nav. Container -->
                                <ul class="mobile-nav">
                                    <li class="responsive-searchbox">
                                        <!-- Responsive Nave -->
                                        <form action="#" method="get">
                                            <input type="text" class="searchbox-inputtext" id="searchbox-inputtext-mobile" name="s" />
                                            <button class="icon-search"></button>
                                        </form>
                                        <!-- //Responsive Nave// -->
                                    </li>
                                </ul>
                                <!-- //Mobile Nav. Container// -->
                            </div>
                            <!-- Nav -->

                        </div>



                    </div>
                </header>
                <!-- //Header// -->