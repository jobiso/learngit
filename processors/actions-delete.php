<?php
include('inc-actions-header.php');

if ($action == "logout"){
	Users::DestroySession();
	$Session_Array = Users::GetSession();
	$User_Id = $Session_Array['User_Id'];
	if ($User_Id){
		header("Location: ../mainmenu.php");
	} else {
		header("Location: ../index.php");
	}
}

if ($action == "delete_system_user"){
	//$ReturnMessage = PatientVisits::Checkout_Patient();
	header("Location: ../manage_system_users.php");
}

if ($action == "delete_premium_item"){
	Orders::Delete_Premium_Item($Id);
	header("Location: ../admin-view-order.php?Order_Id=$Order_Id&message=success");
}

if ($action == "delete_user"){
	Users::Delete_User($user_id);
	header("Location: ../view-users.php?message=success");
}

exit;
?>
