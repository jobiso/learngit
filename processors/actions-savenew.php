<?php
include('inc-actions-header.php');

if ($action == "add_new_user"){
	$Customer_Id = Customers::Add_New_Customer();
	if ($Customer_Id >= 1){
		header("Location: ../view-users.php?msg=success");		
	} else {
		$Return = "add-new-user.php?error=$Customer_Id&First_Name=$First_Name&Last_Name=$Last_Name&Company_Name=$Company_Name&Address=$Address&Town=$Town&Post_Code=$Post_Code&ID_Number=$ID_Number&County=$County&Email=$Email&Mobile_Number=$Mobile_Number&Client_Type=$Client_Type";
		header("Location: ../$Return");		
	}	
}
if ($action == "add_new_manager"){
	$CUSTID = Managers::Add_New_Manager();
	if ($CUSTID >= 1){
		header("Location: ../manage-saccos.php?msg=new_manager_added");		
	} else {
		$Return = "add-sacco-managers.php?Selected_Sacco_Id=$Sacco_Id&error=$CUSTID&First_Name=$First_Name&Last_Name=$Last_Name&Company_Name=$Company_Name&Address=$Address&Town=$Town&Post_Code=$Post_Code&ID_Number=$ID_Number&County=$County&Email=$Email&Mobile_Number=$Mobile_Number&Client_Type=$Client_Type";
		header("Location: ../$Return");		
	}	
}

if ($action == "add_new_boda"){
	$Return_Id = Bodas::Add_New_Bodaboda();
	if ($Return_Id >= 1){
		header("Location: ../view-bodas.php?msg=success");		
	} else {
		$b=serialize($_GET);
		$Return = "add-new-boda.php?error=$Return_Id&b=$b";
		header("Location: ../$Return");		
	}	
}

if ($action == "add_new_sacco"){
	$Return_Id = Saccos::Add_New_Sacco();
	if ($Return_Id >= 1){
		header("Location: ../manage-saccos.php?msg=success");		
	} else {
		$b=serialize($_GET);
		$Return = "manage-saccos.php?error=$Return_Id&b=$b";
		header("Location: ../$Return");		
	}	
}

exit;
?>