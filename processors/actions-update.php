<?php
include('inc-actions-header.php');

if ($action == "process_loan"){
	Loans::Process_Loan($Loan_Id);		
	header("Location: ../view-loans.php?message=loan-updated");
}

if ($action == "edit_user"){
	Users::Update_User();
	header('Location:../view-users.php?message=success');
}

if ($action == "update_order"){
	$return = Orders::UpdateOrder();
	header("Location: ../manage_orders.php?message=success");
}

if ($action == "update_current_account"){
	$return = Users::UpdateSession();
	header("Location: ../mainmenu.php");
}

if ($action == "accept_order"){
	Orders::Accept_Order($Order_Id);
	header("Location: ../print_certificate.php?Order_Id=$Order_Id");
}

if ($action == "admin-edit-customer"){
	Customers::Update_Customer();
	header('Location:../admin-customers.php?message=success');
}

if ($action == "assign_instructor"){	//In dashboard, assign new instructor: Saturday, September 30, 2017
	Jobs::Update($job_id);
	header("Location: ../dashboard.php?message=success");
}

if ($action == "change_rider"){
	Bodas::Update($boda_id);
	header("Location: ../view-bodas.php?message=success");
}

exit;
?>
