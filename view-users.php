<?php
include('header.php');
?>

<?php
$EnableDataTables = "True";
$Dashboard_Section = "View Users";
$Page_Flag = "Agent_View_Customers";
include('inc-dashboard.php');
?>  

<div class="row">
						<div class="col-md-12">

				<div class="card">
                            <div class="header">
                                <h4 class="title">View Users</h4>
                                <p class="category">List of users</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Signup Date</th>
                                            <th>Name</th>
											<th>Sacco</th>
											<th>User Type</th>
											<!-- <th>Ratings</th> -->
                                            <th>Action</th>
											<th>View Trips</th>
                                        </tr>
                                    </thead>
									
                                    <tbody>
									<?php
									if ($User_Type) {
										$Customers_Array = Customers::Get_Customers($User_Type);
									} else {
										$Customers_Array = Customers::Get_Customers();
									}
									$Number = 0;
									foreach ($Customers_Array as $key => $val){
										$Number++;
										$No = number_format($Number);
										echo "
										<tr>
                                            <td>$No</td>
                                            <td>$val[Signup_Date]</td>
                                            <td>";
											if ($val['Company_Name']){
												echo "$val[Company_Name]";
											} else {
												echo "$val[first_name] $val[last_name]";
											}

											$Sacco_Id = $val['Sacco_Id'];

											$Sacco = Saccos::Get_Single_Sacco($Sacco_Id);
											echo "<br>Mobile: 0".$val['mobile_number']."
											</td>
											
											<td>$Sacco[Sacco_Name] - $Sacco[Sacco_Location]</td>
											<td>$val[type]</td>
                                            <td><a href='edit-user.php?user_id=$key'>Edit User</a><br><br><a href='processors/actions-delete.php?action=delete_user&user_id=$key' style='color:red'>Delete User</a></td>
											 <td><a href='view-trips.php'>View User Trips</a></td>
                                        </tr>";
									}
									?>                                  
                                    </tbody>
                                </table>
								</div>
		</div>

		</div>
		</div>
								
<?php
include('inc-mainpage-closer.php');
?>

<?php
include('footer.php');
?>